FROM ubuntu:22.04
WORKDIR /usr/src/cache
LABEL Kennedy Sanchez @SappoTech
RUN apt-get update && apt-get update && apt-get install -y python3.10 python-pip
RUN echo "alias python=python3.10" >> ~/.bashrc
RUN export PATH=${PATH}:/usr/bin/python3.10
RUN /bin/bash -c "source ~/.bashrc"
RUN apt install python3-pip -y
RUN python3.10 -m pip install --upgrade pip
RUN pip install flask
COPY app.py /opt/
ENTRYPOINT FLASK_APP=/opt/app.py flask run --host=0.0.0.0 --port=8080
